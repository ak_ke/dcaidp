''' --------------------------------------------------------------- ***
    [dCaiDP: Calcium influx dependent plasticity]
    Simulation for triplet spike simulation, replicating triplet STDP results

    Simulation script accompanying the manuscript:
    Houben, A.M. & Keil, M.S., (under review). A calcium-influx-dependent plasticity model exhibiting multiple STDP curves

    contact:
        Akke Mats Houben (akkehouben[at]gmail.com)
        MS Keil (matskeil[at]ub.edu)
*** --------------------------------------------------------------- '''

import numpy as np
import matplotlib.pyplot as plt

import sys

tr = 1
if len(sys.argv) > 1:
    tr = int(sys.argv[1])

if tr == 1:
    print('1 pre, 2 post')
elif tr == 2:
    print('2 pre, 1 post')
else:
    print('usage: tripletCaiDP [tr], now using 1 pre, 2 post')

Dt = 0.1    # time-step
Nt = int(2000 / Dt) # simulation time per spike triplet

T = int(300 / Dt)
Tmin = -30/Dt; Tmax = -Tmin+1/Dt; dTpp = 1./Dt
Npp = int((Tmax-Tmin)/dTpp)

# init post-synaptic neuron:
tau_Vm = 10;    # time constant
tau_Af = 3.5;    # Af time constants
tau_As = 8;      # As time constant
tau_cCa = 530;  # [Ca] time contant

# membrane eq. parameters:
Eth = -30; Ereset = -70;
gl = 1; El = -65;
tau_Vlp = 10/9.;  # dendritic AP propagation
tau_G = 20;  # NMDA unblocking speed
# NMDA:
gn_max = 0.01; En = 0;
tau_pN0 = 1.5; tau_pN1 = 152;    # Dayan & Abbot, 2001
# AMPA:
ga_max = 0.05; Ea = 60;
tau_pA = 5.26;    # Dayan & Abbot, 2001

phif = 1e-5; phis = 0.1; alphas = 7.4e-6

def H(x):
    return 1*(x>=0);

def Gnmda(V):   # eq. 7
    cMg = 1;
    return np.power(1+cMg/3.56 * np.exp(-V/16.12), -1);

Ts = np.arange(Tmin, Tmax, dTpp)
W = np.ones((Npp, Npp))*.5

for i in np.arange(Npp):
    for j in np.arange(Npp):
        print('%d, %d (%d, %d)' % (i, j, Npp, Npp))

        # init neuron:
        V = np.zeros(Nt);   V[0] = El;
        ICa = np.zeros(Nt); ICa[0] = 0;
        cCa = np.zeros(Nt); cCa[0] = 0;
        Vlp = np.zeros(Nt); Vlp[0] = V[0];

        gn = np.zeros(Nt);  gn[0] = 0;
        Pnmda = np.zeros(Nt);   Pnmda[0] = 0;
        pN0 = np.zeros(Nt);     pN0[0] = 1; # P+
        pN1 = np.zeros(Nt);     pN1[0] = 0; # P-
        G = np.zeros(Nt);       G[0] = 0;

        ga = np.zeros(Nt);      ga[0] = 0;
        Pampa = np.zeros(Nt);   Pampa[0] = 0;
        dw = np.zeros(Nt);

        Af = np.zeros(Nt);      Af[0] = 0;
        As = np.zeros(Nt);      As[0] = 0;

        if tr == 1: # 1pre-2post
            Spost1 = 0;
            Spost2 = 0;
            Spre = 0;
            Tpost1 = Tmin+dTpp*np.min([i, j]);
            Tpost2 = Tmin+dTpp*np.max([i, j]);
            Tpre = T;

        elif tr == 2:   # 2pre-1post
            Spre1 = 0;
            Spre2 = 0;
            Spost = 0;
            Tpre1 = Tmin+dTpp*i
            Tpre2 = Tmin+dTpp*j
            Tpost = T;

        for n in np.arange(1, Nt):
            preactivity = 0;
            postactivity = 0;

            if tr == 1: # 1pre-2post
                if n == Tpre:
                    preactivity = 1
                if n > (Tpre + Tpost1) and Spost1 == 0:
                    postactivity = 1;
                elif n > (Tpre + Tpost2) and Spost2 == 0:
                    postactivity = 1;

            elif tr == 2:   # 2pre-1post
                if n > Tpost and Spost == 0:
                    postactivity = 1
                if n == (Tpost + Tpre1):
                    preactivity = 1;
                if n == (Tpost + Tpre2):
                    preactivity = 1;
    
            I = 10/Dt*postactivity

            # NMDA:
            dpN0 = (1-pN0[n-1])/tau_pN0 + -0.5*preactivity*(pN0[n-1])/Dt    # eq. 5
            dpN1 = -pN1[n-1]/tau_pN1 + 0.5*preactivity*(1-pN1[n-1])/Dt      # eq. 6
            pN0[n] = pN0[n-1] + Dt*dpN0;
            pN1[n] = pN1[n-1] + Dt*dpN1;
            Pnmda[n] = pN0[n]*pN1[n];
            #G[n] = G[n-1] + Dt*(-G[n-1] + Gnmda(Vlp[n-1]))/tau_G;
            G[n] = (1-1/(tau_G/Dt))*G[n-1] + 1/(tau_G/Dt)*Gnmda(Vlp[n-1]);    # eq. 8
            gn[n] = gn_max*Pnmda[n]*G[n]    # eq. 4

            # AMPA:
            Pampa[n] = Pampa[n-1] + Dt*(-Pampa[n-1]/tau_pA + 0.5*preactivity*(1-Pampa[n-1])/Dt);
            ga[n] = W[i,j]*ga_max*Pampa[n];  # eq. 10

            # Ca:
            ICa[n] = (1-cCa[n-1])*gn[n]*(Vlp[n-1]-En)/gn_max;   # eq. 3

            dcCa = -10*ICa[n];  # eq. 9, without saturation
            cCa[n] = cCa[n-1] + Dt*(-cCa[n-1] + dcCa)/tau_cCa;

            dAf = -Af[n-1] + (1-Af[n-1])*((-ICa[n])-(-ICa[n-1]));   # eq. 13
            Af[n] = Af[n-1] + Dt*dAf/tau_Af;

            dAs = -As[n-1]+ (-ICa[n]); # eq. 12
            As[n] = As[n-1] + Dt*dAs/tau_As;

            dw[n] = Af[n]*H(abs(Af[n])-phif) - alphas*As[n]*H(As[n]-phis);  # eq. 11


            W[i,j] = W[i,j] + dw[n];

            # V:
            v = V[n-1]; # dummy variable to keep peaks in trace of V
            if v > Eth:
                # do spike stuff
                v = Ereset;

            dV = I - gl*(v-El) - ga[n]*(v-Ea) - gn[n]*(v-En);   # eq. 1
            V[n] = v + Dt*dV/tau_Vm;
            Vlp[n] = (1-1/tau_Vlp)*Vlp[n-1] + 1/tau_Vlp*V[n];   # eq. 2

            if V[n] > Eth:
                V[n] = Ereset;
                if tr == 1:
                    if n > (Tpre + Tpost1) and Spost1 == 0:
                        Spost1 = 1
                    if n > (Tpre + Tpost2) and Spost2 == 0:
                        Spost2 = 1
                elif tr == 2:
                    if n > Tpost and Spost == 0:
                        Spost = 1


W = W - 0.5;
plt.imshow(W)

plt.show()

fn = 'triplet_'
if tr == 1:
    fn += '1pre_2post.txt'
elif tr == 2:
    fn += '2pre_1post.txt'
with open(fn, 'w') as f:
    for i in np.arange(Npp):
        for j in np.arange(Npp):
            f.write('%d\t%d\t%.5f\n' % (Ts[i], Ts[j], W[i,j]))
