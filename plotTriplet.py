''' --------------------------------------------------------------- ***
    [dCaiDP: Calcium influx dependent plasticity]
    Script to plot results of tripletdCaiDP.py

    Simulation script accompanying the manuscript:
    Houben, A.M. & Keil, M.S., (under review). A calcium-influx-dependent plasticity model exhibiting multiple STDP curves

    contact:
        Akke Mats Houben (akkehouben[at]gmail.com)
        MS Keil (matskeil[at]ub.edu)
*** --------------------------------------------------------------- '''

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as pltc

import sys

plotFake = 0
tr = 1
if len(sys.argv) > 1:
    tr = int(sys.argv[1])

if tr == 1:
    fn = 'triplet_1pre_2post.txt'
    xlabel = '$t_{post_2}-t_{pre} (ms)$'
    ylabel = '$t_{post_1}-t_{pre} (ms)$'
elif tr == 2:
    fn = 'triplet_2pre_1post.txt'
    xlabel = '$t_{pre_2}-t_{post} (ms)$'
    ylabel = '$t_{pre_1}-t_{post} (ms)$'

# read in data:
Tsi = np.array([]); Tsj = np.array([]); W_ = np.array([]);
with open(fn, 'r') as f:
    for line in f.readlines():
        Tsi = np.append(Tsi, int(line.split('\t')[0]))
        Tsj = np.append(Tsj, int(line.split('\t')[1]))
        W_ = np.append(W_, float(line.split('\t')[2]))

Ts = np.sort(np.unique(Tsi))
W = np.zeros((len(Ts), len(Ts)))
for k in np.arange(len(W_)):
    i = np.where(Ts == Tsi[k])[0][0];
    j = np.where(Ts == Tsj[k])[0][0];
    W[i,j] = W_[k]

def win(x):
    return -50 * np.exp(-x/100.) * (x>0) + 50*np.exp(x/100.) * (x<=0)

if plotFake:
    for i in np.arange(len(Ts)):
        for j in np.arange(len(Ts)):
            Dt = 0.1
            if tr == 1:
                W[i,j] = win(-Ts[i]) + win(-Ts[j])
            elif tr == 2:
                W[i,j] = win(Ts[i]) - win(-Ts[j])

Wplt = np.zeros((np.shape(W)[0], np.shape(W)[1], 4))
W = W / np.max(abs(W))
for i in np.arange(len(W)):
    for j in np.arange(len(W)):
        w = W[i,j] / np.max(abs(W))
        r = (w>0)
        g = 0
        b = (w<0)
        a = np.abs(w)
        Wplt[i,j] = (r, g, b, a);

im = plt.imshow(Wplt, origin='lower')
ax = plt.gca()
ax.set_xticks(np.arange(0, len(Ts), 4))
ax.set_yticks(np.arange(0, len(Ts), 4))
tickfont = {'fontsize': 12};
ax.set_xticklabels(np.array(Ts[::4]/10, dtype=int), fontdict=tickfont)
ax.set_yticklabels(np.array(Ts[::4]/10, dtype=int), fontdict=tickfont)
lblfont = {'fontsize': 15};
ax.set_xlabel(xlabel, fontdict=lblfont)
ax.set_ylabel(ylabel, fontdict=lblfont)
# construct cb:
Wmin = np.min(W); Wmax = np.max(W)
cbN = 100; cbW = 4
ix = np.arange(Wmin*cbN, Wmax*cbN)
r = np.repeat(np.mat(ix>0).T, cbW, 1)
g = np.repeat(np.mat(ix*0).T, cbW, 1); 
b = np.repeat(np.mat(ix<0).T, cbW, 1)
a = np.repeat(np.mat(abs(ix/cbN)).T, cbW, 1)
cbc = np.zeros((int(np.max(ix)-np.min(ix)+1), cbW, 4));
cbc[:,:,0] = r; cbc[:,:,1] = g; cbc[:,:,2] = b; cbc[:,:,3] = a
cb = plt.colorbar()
cb.ax.cla();
cb.ax.imshow(cbc)
# ticks:
tickStep = 16
cb.set_ticks(np.arange(0, len(ix), tickStep))

cb.set_ticklabels([str(round(float(x), 2)) for x in ix[::tickStep]/cbN])
cb.ax.set_xticklabels(''); 
cb.ax.invert_yaxis() 

        
plt.show()


'''
# save figure:
if tr == 1:
    outfn = 'Figtriplet_1pre2post.pdf'
elif tr == 2:
    outfn = 'Figtriplet_2pre1post.pdf'
if plotFake:
    if tr == 1:
        outfn = 'FigFaketriplet_1pre2post.pdf'
    elif tr == 2:
        outfn = 'FigFaketriplet_2pre1post.pdf'

plt.savefig(outfn, format='pdf')
import os

os.system('pdftops -eps ' + outfn + ' ' + outfn.split('.pdf')[0] + '.eps')
'''
