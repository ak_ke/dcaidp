''' --------------------------------------------------------------- ***
    [dCaiDP: Calcium influx dependent plasticity]
    Simulation for single pre-post pairing simulation, replicating classical STDP curves.

    Simulation script accompanying the manuscript:
    Houben, A.M. & Keil, M.S., (under review). A calcium-influx-dependent plasticity model exhibiting multiple STDP curves

    contact:
        Akke Mats Houben (akkehouben[at]gmail.com)
        MS Keil (matskeil[at]ub.edu)
*** --------------------------------------------------------------- '''

import numpy as np
import matplotlib.pyplot as plt

Dt = 0.1    # time step
Nt = int(2000 / Dt) # total simulation time (ms*Dt)

# stimulation:
T = int(300 / Dt)                       # post-synaptic spike time
Tmin = -100/Dt; Tmax = -Tmin; dTpp = 1./Dt; # min, max stimulation pairings; step
Npp = int((Tmax-Tmin)/dTpp);            # # of pairings

# init post-synaptic neuron:
tau_Vm = 10;    # time constant
tau_Af = 3.5;    # Af time constants
tau_As = 8;      # As time constant 
tau_cCa = 530;  # [Ca] time contant

# membrane eq. parameters:
Eth = -30; Ereset = -70;
gl = 1; El = -65;
tau_Vlp = 10/9.;  # dendritic AP propagation
tau_G = 20;  # NMDA unblocking speed
# NMDA:
gn_max = 0.01; En = 0; 
tau_pN0 = 1.5; tau_pN1 = 152;    # Dayan & Abbot, 2001
# AMPA:
ga_max = 0.05; Ea = 60;
tau_pA = 5.26;    # Dayan & Abbot, 2001

# dW parameters:
alpha_Af = 0.1;
phif = 1e-5; phis = 0.1; alphas = 7.4e-6

def H(x):
    return 1*(x>=0);

def Gnmda(V):   # eq. 7
    cMg = 1;
    return np.power(1+cMg/3.56 * np.exp(-V/16.12), -1);

Ts = np.arange(Tmin, Tmax, dTpp);
W = np.zeros((Npp, Nt));
W[:, 0] = 0.5;
As_ = np.zeros((Npp, Nt));
Af_ = np.zeros((Npp, Nt));
cCa_ = np.zeros((Npp, Nt));
ICa_ = np.zeros((Npp, Nt));
dw_ = np.zeros((Npp, Nt));

for i in np.arange(Npp):
    print("%d / %d" % (i, Npp))
    # neuron variables:
    V = np.zeros(Nt);       V[0] = El;
    ICa = np.zeros(Nt);     ICa[0] = 0;
    cCa = np.zeros(Nt);     cCa[0] = 0;
    Vlp = np.zeros(Nt);     Vlp[0] = V[0];

    gn = np.zeros(Nt);      gn[0] = 0;
    Pnmda = np.zeros(Nt);   Pnmda[0] = 0;
    pN0 = np.zeros(Nt);     pN0[0] = 1; # P+
    pN1 = np.zeros(Nt);     pN1[0] = 0; # P-
    G = np.zeros(Nt);       G[0] = 0;

    ga = np.zeros(Nt);      ga[0] = 0;
    Pampa = np.zeros(Nt);   Pampa[0] = 0;
    dw = np.zeros(Nt);

    Af = np.zeros(Nt);      Af[0] = 0;
    As = np.zeros(Nt);      As[0] = 0;

    Tpre = Ts[i];
    postSpike = 0;  spikeT = Nt;
    for n in np.arange(1, Nt):   # n -> Nt
        preactivity = 0;
        postactivity = 0;
        if n > T and postSpike == 0:
            postactivity = 1
        if Tpre <= 0:
            if n == T+Tpre:
                preactivity = 1;
        else:
            if n == spikeT+Tpre:
                preactivity = 1;

        I = 10/Dt*postactivity;

        # update of neuron: ------------------------------------------
        # NMDA:
        dpN0 = (1-pN0[n-1])/tau_pN0 + -0.5*preactivity*(pN0[n-1])/Dt    # eq. 5
        dpN1 = -pN1[n-1]/tau_pN1 + 0.5*preactivity*(1-pN1[n-1])/Dt      # eq. 6
        pN0[n] = pN0[n-1] + Dt*dpN0;
        pN1[n] = pN1[n-1] + Dt*dpN1;
        Pnmda[n] = pN0[n]*pN1[n];

        #G[n] = G[n-1] + Dt*(-G[n-1] + Gnmda(Vlp[n-1]))/tau_G;
        G[n] = (1-1/(tau_G/Dt))*G[n-1] + 1/(tau_G/Dt)*Gnmda(Vlp[n-1]);    # eq. 8
        gn[n] = gn_max*Pnmda[n]*G[n]    # eq. 4

        # AMPA:
        Pampa[n] = Pampa[n-1] + Dt*(-Pampa[n-1]/tau_pA + 0.5*preactivity*(1-Pampa[n-1])/Dt);
        ga[n] = W[i][n-1]*ga_max*Pampa[n];  # eq. 10
        
        # Ca:
        ICa[n] = (1-cCa[n-1])*gn[n]*(Vlp[n-1]-En)/gn_max;   # eq. 3

        dcCa = -10*ICa[n];  # eq. 9
        cCa[n] = cCa[n-1] + Dt*(-cCa[n-1] + dcCa)/tau_cCa;

        # weight update:
        dAf = -Af[n-1] + (1-Af[n-1])*alpha_Af*((-ICa[n])-(-ICa[n-1]))/Dt;   # eq. 13
        Af[n] = Af[n-1] + Dt*dAf/tau_Af;

        dAs = -As[n-1]+ (-ICa[n]); # eq. 12
        As[n] = As[n-1] + Dt*dAs/tau_As;

        dw[n] = Af[n]*H(abs(Af[n])-phif) - alphas*As[n]*H(As[n]-phis);  # eq. 11
        W[i][n] = W[i][n-1] + dw[n];

        # V:
        v = V[n-1]; # dummy variable to keep peaks in trace of V
        if v > Eth:
            # do spike stuff
            v = Ereset;

        dV = I - gl*(v-El) - ga[n]*(v-Ea) - gn[n]*(v-En);   # eq. 1
        V[n] = v + Dt*dV/tau_Vm;
        #Vlp[n] = Vlp[n-1] + Dt*(-Vlp[n-1]+V[n]) / tau_Vlp;
        #Vlp[n] = Vlp[n-1] + Dt*(-Vlp[n-1]+V[n]) / tau_Vlp;
        Vlp[n] = (1-1/tau_Vlp)*Vlp[n-1] + 1/tau_Vlp*V[n];   # eq. 2

        # end: update of neuron --------------------------------------

        if V[n] > Eth:
            postSpike = 1;
            spikeT = n;
            V[n] = Ereset;

    # save Calcium derived signals:
    As_[i,:] = As;
    Af_[i,:] = Af;
    cCa_[i,:] = cCa;
    ICa_[i,:] = ICa;
    dw_[i,:] = dw;

# plotting:
w = W[:,-1];
w = w-0.5;
plt.plot(Ts, w / np.max(abs(w)), '-*');
plt.ylim(-1, 1)
plt.grid()
plt.show()

'''
# write results to files in order to construct figures:
with open('./results/w.txt', 'w') as f:
    for t, w_ in zip(Ts, w/np.max(abs(w))):
        f.write('%d\t%.5f\n' % (t, w_))

time = np.arange(0, Nt*Dt, Dt)
with open('./results/cCaLTP.txt', 'w') as f:
    for t, c in zip(time, cCa_[80,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/cCaLTD.txt', 'w') as f:
    for t, c in zip(time, cCa_[120,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/ICaLTP.txt', 'w') as f:
    for t, c in zip(time, ICa_[80,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/ICaLTD.txt', 'w') as f:
    for t, c in zip(time, ICa_[120,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/AfLTP.txt', 'w') as f:
    for t, c in zip(time, Af_[80,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/AfLTD.txt', 'w') as f:
    for t, c in zip(time, Af_[120,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/AsLTP.txt', 'w') as f:
    for t, c in zip(time, As_[80,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/AsLTD.txt', 'w') as f:
    for t, c in zip(time, As_[120,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/dwLTP.txt', 'w') as f:
    for t, c in zip(time, dw_[80,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/dwLTD.txt', 'w') as f:
    for t, c in zip(time, dw_[120,:]):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/dwLTPcsum.txt', 'w') as f:
    for t, c in zip(time, np.cumsum(dw_[80,:])):
        f.write('%.2f\t%.5f\n' % (t, c))
with open('./results/dwLTDcsum.txt', 'w') as f:
    for t, c in zip(time, np.cumsum(dw_[120,:])):
        f.write('%.2f\t%.5f\n' % (t, c))
'
'''
